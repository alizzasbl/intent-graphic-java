package com.example.asus_rog.belajarinputgambar;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Register extends AppCompatActivity implements View.OnClickListener {

    Button Register2;
    EditText Name,Pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Name = findViewById(R.id.Username);
        Pass = findViewById(R.id.Password);

        Register2 = findViewById(R.id.btnRegister2);
        Register2.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(Register.this, Login.class);
        intent.putExtra("Name", Name.getText().toString());
        intent.putExtra("Password", Pass.getText().toString());
        startActivity(intent);
    }
}
