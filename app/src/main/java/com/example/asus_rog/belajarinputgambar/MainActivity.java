package com.example.asus_rog.belajarinputgambar;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.widget.ImageView;

import static android.graphics.Bitmap.Config.ARGB_8888;

public class MainActivity extends Activity {

    ImageView ourView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        draw();

         setContentView(ourView);

    }

    public void draw(){


        Bitmap bitmap;
        bitmap = Bitmap.createBitmap(600,600, ARGB_8888);
        Canvas canvas;
        canvas = new Canvas(bitmap);

        ourView = new ImageView(this);
        ourView.setImageBitmap(bitmap);

        Paint paint;
        paint = new Paint();

        // Memberikan warna kanvas
        canvas.drawColor(Color.argb(100, 128, 0, 0));

        // Mengatur warna
        paint.setColor(Color.argb(100,  192, 192, 192));
        // We can change this around as well


        Bitmap bitmapPic;
        // insialisasi gambar
        bitmapPic = BitmapFactory.decodeResource(this.getResources(), R.drawable.bunga);

        // Posisi gambar
        canvas.drawBitmap(bitmapPic, 100, 150, paint);

        // Membuat Garis
        canvas.drawLine(250,250,400,600,paint);

        // membuat Tulisan
        canvas.drawText("Welcome Strangers!!", 250, 500, paint);

        // membuat lingkaran
        canvas.drawCircle(350,170,200,paint);

        // mengubah warna
        paint.setColor(Color.argb(50,  32, 178, 170));

        // membuat kotak
        canvas.drawRect(30,250,250,300,paint);

        // mengubah warna
        paint.setColor(Color.argb(50,  255, 255, 0));

        }
}