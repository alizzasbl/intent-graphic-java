package com.example.asus_rog.belajarinputgambar;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Login extends AppCompatActivity implements View.OnClickListener {

    Button Login;
    Button Register1;
    EditText email,password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Register1 = findViewById(R.id.btnRegister1);
        Register1.setOnClickListener(this);

        Login = findViewById(R.id.btnLogin);
        Login.setOnClickListener(this);

        email = findViewById(R.id.UsernameLogin);
        password = findViewById(R.id.PasswordLogin);

        email.setText(getIntent().getStringExtra("Name"));
        password.setText(getIntent().getStringExtra("Password"));
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.btnRegister1:
                Intent intent = new Intent(Login.this, Register.class);
                startActivity(intent);
                break;
            case R.id.btnLogin:
                if (!email.getText().toString().equals("")&&(!password.getText().toString().equals(""))) {
                    intent = new Intent(Login.this, MainActivity.class);
                    startActivity(intent);
                }
        }


    }
}
